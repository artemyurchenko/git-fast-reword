import argparse
import os
import sys
import csv

from typing import Optional, List, Iterable, Mapping, Dict, NewType, Union

import pygit2
from pygit2 import Repository, Commit, Oid
import git_filter_repo

StrOid = NewType("StrOid", str)


def get_str_oid(commit: Union[Commit, git_filter_repo.Commit]) -> StrOid:
    if isinstance(commit, Commit):
        return StrOid(commit.oid.hex)
    else:
        return StrOid(commit.original_id.decode("ascii"))


def fallback_reword(rewordings: Mapping[StrOid, str]):
    # git_filter_repo is a BFG, though fast. Consider 'git-format-patch' and 'git-am'.

    args = git_filter_repo.FilteringOptions.parse_args(["--force", "--refs", "HEAD"])
    commit_callback = mk_commit_callback(rewordings)
    filter = git_filter_repo.RepoFilter(args, commit_callback=commit_callback)
    filter.run()


def mk_commit_callback(rewordings: Mapping[StrOid, str]):
    def inner(commit: git_filter_repo.Commit, *_):
        rewording = rewordings.get(get_str_oid(commit))
        if rewording:
            commit.message = rewording.encode()

    return inner


def try_inplace_reword(repo: Repository, rewordings: Mapping[StrOid, str]) -> bool:
    trail = get_trail_of_commits(repo, rewordings.keys())
    if trail is None:
        return False
    if not trail:
        return True

    prev_oid = None
    for commit in trail:
        parent_ids = [prev_oid] if prev_oid else commit.parent_ids
        message = rewordings.get(get_str_oid(commit), commit.message)
        prev_oid = make_commit_from(
            repo, commit, message=message, parent_ids=parent_ids
        )

    assert prev_oid
    repo.head.set_target(prev_oid, "fast-reword")
    return True


def get_trail_of_commits(
    repo: Repository, commits: Iterable[StrOid]
) -> Optional[List[Commit]]:
    not_found = set(commits)
    current = repo.head.peel()
    trail = []
    while not_found:
        trail.append(current)
        not_found.discard(get_str_oid(current))
        if not current.parents:
            break
        current = current.parents[0]  # fixme, work with merges
    if not_found:
        return None
    trail.reverse()
    return trail


def make_commit_from(repo: Repository, commit: Commit, **kwargs) -> Oid:
    props = [
        "reference_name",
        "author",
        "committer",
        "message",
        "tree_id",
        "parent_ids",
    ]
    values = [kwargs.get(prop, getattr(commit, prop, None)) for prop in props]
    return repo.create_commit(*values)


def reword_commits(
    repo: Repository, rewordings: Mapping[StrOid, str], use_filter_repo=False
):
    if use_filter_repo:
        fallback_reword(rewordings)
        return

    if try_inplace_reword(repo, rewordings):
        print("success")
    else:
        print("inplace failed, falling back to git-filter-repo")
        fallback_reword(rewordings)


def read_rewordings(repo, a_file) -> Dict[StrOid, str]:
    res = {}
    with open(a_file, "r", newline="") as csvfile:
        reader = csv.reader(csvfile)
        for i, row in enumerate(reader):
            if not row:
                continue
            if len(row) != 2:
                report_error(
                    f"Rows in a file should be in the format 'REF, MSG', but the row #{i} does not comply"
                )
            rev, msg = row
            commit = repo.revparse_single(rev)
            res[get_str_oid(commit)] = msg + "\n"
    return res


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--use-filter-repo",
        action="store_true",
        help="Use fallback reword via git-filter-repo",
    )

    subparsers = parser.add_subparsers(dest="subcommand", required=True)

    parser_one_commit = subparsers.add_parser("one-commit", help="Reword one commit")
    parser_one_commit.add_argument(
        "-m", "--msg", required=True, help="New message for the commit"
    )
    parser_one_commit.add_argument(
        "<pathspec>", help="Specification of a commit to reword"
    )

    parser_from_csv = subparsers.add_parser(
        "from-csv", help="Reword multiple commits specified in a CSV file"
    )
    parser_from_csv.add_argument(
        "<csvfile>", help="A CSV file mapping revisions to their new messages"
    )

    args = parser.parse_args()
    vargs = vars(args)

    cwd = os.getcwd()
    repo_path = pygit2.discover_repository(cwd)
    if not repo_path:
        report_error(
            "No git repo found in the current working directory (or any parent)"
        )
    repo = Repository(repo_path)

    if args.subcommand == "one-commit":
        commit = repo.revparse_single(vargs["<pathspec>"])
        message = args.msg + "\n"
        reword_commits(repo, {get_str_oid(commit): message}, args.use_filter_repo)
    elif args.subcommand == "from-csv":
        rewordings = read_rewordings(repo, vargs["<csvfile>"])
        reword_commits(repo, rewordings, args.use_filter_repo)
    else:
        report_error(f"unknown subcommand: '{args.subcommand}'")


def report_error(msg: str):
    sys.exit("error: " + msg)


if __name__ == "__main__":
    main()
