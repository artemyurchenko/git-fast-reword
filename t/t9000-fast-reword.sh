#!/bin/bash

test_description='git-fast-reword

This test exercises git-fast-reword correctness'

ROOT="$(pwd)/.."
TEST="$(pwd)/t9000"
TEMP="$(mktemp -d)"

. ./test-lib.sh

function simple_reword() {
    EXPECTED=$1
    shift
    ARGS=("$@")

    NAME="fast-reword using '${ARGS[@]}'"
    test_expect_success "$NAME" '
        cd $TEMP &&
        echo "test repo dir is: $TEMP" &&
        git init &&
        git fast-import --done --force < $TEST/basic &&
        python3 "$ROOT/main.py" "${ARGS[@]}" &&
        git fast-export --all --use-done-feature > ACTUAL &&
        test_cmp $TEST/$EXPECTED ACTUAL
    '
}

simple_reword basic-B one-commit -m "my message" HEAD
simple_reword basic-A one-commit -m "my message" HEAD^
simple_reword basic-A-and-B from-csv "$TEST/basic-A-and-B.csv"

simple_reword basic-B --use-filter-repo one-commit -m "my message" HEAD
simple_reword basic-A --use-filter-repo one-commit -m "my message" HEAD^
simple_reword basic-A-and-B --use-filter-repo from-csv "$TEST/basic-A-and-B.csv"

test_done
